#!/usr/bin/python3

"""Main function wrapper"""

import sys

from .cli import parse_args
from .scan import scan_wrapper
from .concerts import bit
from .api.bandsintown import BandsintownError
from .output import output_wrapper


def main(argv=sys.argv[1:]):  # pylint: disable=W0102
    """Main function."""
    args = parse_args(argv)
    artist_list = scan_wrapper(args.directory, args.cache_dir)
    try:
        concert_list = bit(artist_list, args)
    except BandsintownError as exc:
        return f"failed to fetch concert list from Bands In Town: {exc}"
    output_wrapper(concert_list, args.output)
    return None


if __name__ == "__main__":
    sys.exit(main())
